// Set to true to use the device's contacts when running under PhoneGap
// Set to false to use the dummy contacts file
var USE_PHONEGAP = false;

// Various timeout values in milliseconds
var SCRIPT_LOAD_TIMEOUT = 3000;
var MAP_LOAD_TIMEOUT = 3000;
var FIT_BOUNDS_TIMEOUT = 500;

function onDeviceReady() {
	console.log("onDeviceReady - Executing onDeviceReady...");
    $(document).ready(function() {
                      console.log("Initialising JQuery...");
                      });
    // do any further PhoneGap related initialisation here
}

//document.addEventListener("deviceready", onDeviceReady);

/**
 *
 * initCityChums()
 *
 * Sets up appropriate event handler to display the cities list
 * when everything is ready
 *
 * Also adds a click event handler to the "choose city" button
 * and a "pageshow" handler to the map page
 *
 * Called when the page body has loaded
 */
function initCityChums() {
    
    console.log("initCityChums - Initialising...");
    
    if (USE_PHONEGAP) {
        console.log("initCityChums - Using PhoneGap...");
        // using PhoneGap: we need to wait for deviceready before trying to
        // access the device's contacts database
        document.addEventListener('deviceready', showCities, false);
    } else {
        console.log("initCityChums - NOT Using PhoneGap...");
        // Not using PhoneGap: We can pull the contacts immediately from the
        // dummy contacts file as soon as the DOM is ready
        $(showCities);
    }
    
    // hook up event handlers for "Choose City" button and "map" page
    console.log("initCityChums - Setting Up Event Handlers...");
    $('#chooseCity').live('click', showCities);
    $('#refreshMap').live('click', refreshMap);
    $('#map').live('pageshow', initMap);

    // check connection to Google Maps API
    console.log("initCityChums - Checking Connection to Google Maps...");
    setTimeout(checkConnection, SCRIPT_LOAD_TIMEOUT);
    
}


/**
 * checkConnection()
 *
 * Checks that the Google Maps script has loaded
 * If not, displays an alert and retires
 *
 */
function checkConnection() {
    if (typeof google == 'undefined') {
        alert("Can't connect to the Google Maps server. Please make sure you are connected and try again");
        window.location.reload(true);
    }
}

/**
 *
 * Redraws the map when the user rotates their device
 * so that markers remain visible
 *
 */
$(window).orientationchange(function(e) {
                            if ($.mobile.activePage.attr('id') == 'map') {
                                window.cityChumsMap.fitBounds(window.cityChumsMapBounds);
                                //window.cityChumsMap("refresh");
                                google.maps.event.trigger(window.cityChumsMap, 'resize');
                            }
                        });

/**
 *
 * refreshMap()
 *
 * Refreshes the Google Map
 * Called when refresh button is pressed
 *
 */
function refreshMap() {
    console.log("refreshMap - Refreshing Map...");
    google.maps.event.trigger(window.cityChumsMap, 'resize');
    window.cityChumsMap.fitBounds(window.cityChumsMapBounds);
}

/**
 *
 * initMap()
 *
 * Creates the Google Map and adds it to the #map page
 * Called when #map page is first displayed
 *
 */
function initMap() {
    console.log("initMap - Setting Up Google Map Options...");
    var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(0,0),
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    console.log("initMap - Seting Up Google Map Element on Page...");
    window.cityChumsMap = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
    
    // prevent map from being created again
    $('#map').die('pageshow');
    
    // refresh map each time page is shown from now on
    //$('#map').live('pageshow', refreshMap);
    
    console.log("initMap - Finished Initialising Google Map...");
}

/**
 * showCities()
 *
 * Shows the list of cities in the Contacts database
 *
 */
function showCities() {
    console.log("showCities - Initialising...");
    var cities = new Array();
    clearTimeout(window.mapLoadTimeout);
    $('#mapLoadingMessage').fadeOut();
    
    if (USE_PHONEGAP) {
            console.log("showCities - Using PhoneGap to get Contacts...");
        // Using PhoneGap: Pull all the contaces from devices Contacts database
        var options = new ContactFindOptions();
        options.filter = "";
        options.multiple = true;
        var contactFields = ["addresses"];
        navigator.contacts.find(contactFields, onSuccess, findContactError, options);
    } else {
            console.log("showCities - Using Dummy Contacts File...");
        // Not using PhoneGap: just fill the array with dummy contacts entries
        for (var i=0; i < dummyContacts.length; i++) {
            cities.push({
                        cityName: dummyContacts[i].city,
                        numContacts: dummyContacts[i].contacts.length
                        });
        }
        console.log("showCities - showing Cities List...");
        showCitiesList();
    }
    
    /**
     * onSuccess
     *
     * Callback for PhoneGap's navigator.contacts.find method
     *
     * Add all the found cities to the array then display the cities list
     *
     * Called when all the contacts have been found
     *
     */
    function onSuccess(contacts) {
            console.log("onSuccess - Initialising...");    
        for (var i=0; i < contacts.length; i++) {
            for (j=0; i < contacts[i].addresses.length; j++) {
                var city = contacts[i].addresses[j].locality;
                // is the city already in the array?
                var found = false;
                for (var k=0; k < cities.length; k++) {
                    if (cities[k].cityName == city) {
                        // Yes, increment the city's contact count
                        found = true;
                        cities[k].numContacts++;
                        break;
                    }
                }
                // No, add the city
                if (!found) cities.push({ cityName: city, numContacts: 1 });
            }
        }
        console.log("onSuccess - Showing Cities List...");
        showCitiesList();
    }
    
    /**
     * showCitiesList()
     *
     * Displays the list of cities as a List View
     * Called when the cities array has been populated
     *
     */
    function showCitiesList() {
        console.log("showCitiesList - Initialising...");
        var citiesList = $('#citiesList');
        citiesList.empty();
        
        console.log("showCitiesList - Setting Up List...");
        for (var i=0; i < cities.length; i++) {
            var li = $('<li><a href="#">' + cities[i].cityName + '<span class="ui-li-count">'
                       + cities[i].numContacts + '</span></a></li>');
            citiesList.append(li);
            
            // Store the city name in the list item
            li.jqmData('city', cities[i].cityName);
            
            console.log("showCitiesList - Added List Item '" + cities[i].cityName + "'..");
            
            // When the user taps the item, display the city's contacts on the map
            li.click(function() { mapContactsInCity($(this).jqmData('city')); });
            
            console.log("showCitiesList - Added Event Handler For List Item '" + li.jqmData('city') + "'...");
        }
        
        console.log("showCitiesList - Displaying List...");
        // Display the cities list and update the list view widget
        $.mobile.changePage('#cities', { transition: "none" });
        citiesList.listview('refresh');
    }

}

/**
 * findContactsError()
 *
 * Callback for PhoneGap's navigator.contacts.find method
 * Called when there was a problem accessing the device's contacts database
 *
 * Displays an error alert to the user
 *
 */
function findContactsError(contactError) {
    alert("There was an error accessing the contacts on your device.");
}

/**
 * mapContactsInCity()
 *
 * Finds all contacts in supplied city, places them on the map,
 * moves the map viewport to encompass the contacts, 
 * and displays the map
 *
 */
function mapContactsInCity(city) {
    console.log("mapContactsInCity - Mapping Contacts In " + city + "...");
    
    // Clear the map bounds and start tracking the number of geocoded contacts
    window.cityChumsMapBounds = new google.maps.LatLngBounds();
    var contactsGeocoded = 0;
    
    console.log("mapContactsInCity - Setting Timeout...");
    // if the map takes too long to display, show the loading message and
    // try displaying the map again
    window.mapLoadTimeout = setTimeout( function() {
                                       $('mapLoadingMessage').fadeIn();
                                       mapContactsInCity(city)
                                       }, MAP_LOAD_TIMEOUT);
    
    // Store the marker objects in a static property (create if doesn't exist)
    if (this.markers == undefined) this.markers = new Array();
    
    console.log("mapContactsInCity - Removing Previous Markers...");
    // Remove any previous markers from the map
    for (var i=0; i < this.markers.length; i++) {
        this.markers[i].setMap(null);
    }
    this.markers.length = 0;
    
    console.log("mapContactsInCity - Transitioning Map Into View...");
    // start transitioning the map page into view
    $('#map h2').html('Contacts In ' + city);
    $.mobile.changePage('#map', { transition: "none", reverse: true });
    
    if (USE_PHONEGAP) {
        console.log("mapContactsInCity - Getting Contacts From Device...");
        // using PhoneGap, pull all contacts from device's contacts database
        var options = new ContactFindOptions();
        options.filter = city;
        options.multiple = true;
        var contactFields = ["displayName", "addresses"];
        navigator.contacts.find(contactFields, onSuccess, findContactsError, options);
    } else {
        console.log("mapContactsInCity - Mapping Dummy Contacts...");
        // Not using PhoneGap, just fill the contacts array with dummy contacts
        for (var i=0; i < dummyContacts.length; i++) {
            if (dummyContacts[i].city == city) {
                var dummyContactsInCity = dummyContacts[i].contacts;
                break;
            }
        }
        onSuccess(dummyContactsInCity);
    }
    
    /**
     * Geocodes each contact address that is within the city
     * Called when all contacts in city have been found
     */
    function onSuccess(contacts) {
        console.log("mapContactsInCity - Geocoding " + contacts.length + " addresses...");
        for (var i=0; i < contacts.length; i++) {
            for (var j=0; j < contacts[i].addresses.length; j++) {
                if (contacts[i].addresses[j].locality == city) {
                    console.log("mapContactsInCity - Geocoding '" + contacts[i].addresses[j].streetAddress + "," + contacts[i].addresses[j].locality + "'...");
                    // found address in city, geocode it
                    var request = {
                        address: contacts[i].addresses[j].streetAddress + " " + city
                    };
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode(request, addMarker(contacts[i].displayName, contacts.length));
                }                
            }
        }
    }

    /**
     * Adds a geocoded address as a marker to the map
     * 
     * Once all contacts have been geocoded, moves the map to encompass the markers
     *
     * Called when an address has been successfully geocoded
     *
     * Since we need to access displayName and numContacts within the callback, we
     * wrap it in an enclosure
     */
    function addMarker(displayName, numContacts) {
        //console.log("addMarker - Adding Map Marker for '" + displayName + "'..."); 
        return function(results, status) {
            if (status == "OVER_QUERY_LIMIT") {
                alert("Too many Google Maps requests... Please try again later...");
            }
            if (status != "OK") return;
            
            // Loop through all the returned results, usually there is only one
            for (var i=0; i < results.length; i++) {
                
                console.log("addMarker - Creating Map Marker for '" + displayName + "'..."); 
                console.log("addMarker - Creating Map Marker at position '" + results[i].geometry.location + "'..."); 
                
                // create the marker
                var marker = new google.maps.Marker({
                                                    position: results[i].geometry.location,
                                                    map: window.cityChumsMap,
                                                    title: displayName
                                                    });
                
                // store the marker and add it to the map
                this.markers.push(marker);
                marker.setMap(window.cityChumsMap);
                
                // store the contact address in the marker so we can
                // display the info window
                marker.formatted_address = results[i].formatted_address;
                
                // extend the map bounds to encompass the new marker
                window.cityChumsMapBounds.extend(results[i].geometry.location);
                
                console.log("addMarker - Adding Event Listener and creating InfoWindow..."); 
                // when the user taps the marker, display an info window
                // containing name and address
                google.maps.event.addListener(marker, 'click', function() {
                                            var address = '<div style="font-size: 80%;"><strong>' + displayName + '</strong><br>';
                                            address += marker.formatted_address;
                                            address += '</div>';
                                            var infowindow = new google.maps.InfoWindow({
                                                                content: address,
                                                                maxWidth: 180
                                                            });
                                              infowindow.open(window.cityChumsMap, marker);
                                        });
                contactsGeocoded++;
                
                // If we've placed all the markers, move the map to encompass the
                // new bounds and remove and Loading message
                if (contactsGeocoded >= numContacts) {
                    console.log("addMarker - Successfully created all Map Markers..."); 
                    setTimeout(function() {
                               console.log("addMarker - Resetting Map Bounds..."); 
                               window.cityChumsMap.fitBounds(window.cityChumsMapBounds);
                               console.log("addMarker - Refreshing Map..."); 
                               window.cityChumsMap("refresh");
                               }, FIT_BOUNDS_TIMEOUT);
                    clearTimeout(window.mapLoadTimeout);
                    $('#mapLoadingMessage').fadeOut();
                }
                
            }
            
        }
    }

} // END mapContactsInCity FUNCTION
                    
